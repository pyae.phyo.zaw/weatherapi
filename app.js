require('dotenv').config();
const express = require('express');
const cors = require('cors');
const APIData = require('./utils/getData');
const app = express();

app.use(express.json());
app.use(cors());
const port = process.env.PORT || 3000;

app.get("/", (req, res) => {
    res.send("Welcome to Main");
});

app.get("/weather", async (req, res) => {
    let city = req.query.city;
    let data = await APIData.getLists(city);
    res.send(data);
});

app.listen(port, () => {
    console.log("Server is running on Port " + port);
})