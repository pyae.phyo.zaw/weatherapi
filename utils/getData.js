const axios = require('axios').default;

const reqObj = {
    method: 'GET',
    url: 'http://api.weatherstack.com/current',
    responseType: 'json',
    params: {
        access_key: '6d799c47209c10219c8a407af5332274',
        query: ""
    }
};

exports.getLists = async (city) => {
    reqObj.params.query = city;
    return axios.request(reqObj).then(result => {
        if (result.data.error) {
            return {
                code: 601,
                error: {
                    type: 'missing_query',
                    info: 'Please specify a valid location'
                },
            }
        } else {
            let data = result.data;
            let exportData = {
                code: 200,
                info: "Success",
                data: {
                    location: {
                        name: data.location.name,
                        country: data.location.country,
                        region: data.location.region,
                    },
                    weatherInfo: {
                        weather_icons: data.current.weather_icons,
                        weather_descriptions: data.current.weather_descriptions,
                    },
                    windInfo: {
                        wind_speed: data.current.wind_speed,
                        wind_degree: data.current.wind_degree,
                        wind_dir: data.current.wind_dir,
                        pressure: data.current.pressure,
                    },
                    temperature: data.current.temperature,
                    observation_time: data.current.observation_time,
                    humidity: data.current.humidity,
                    cloudcover: data.current.cloudcover,
                    uv_index: data.current.uv_index,
                    visibility: data.current.visibility,
                    is_day: data.current.is_day
                }
            };
            return exportData;
        }
    }).catch(e => {
        console.log(e);
    })
};




